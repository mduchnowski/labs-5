﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.GImplementation;
using lab5.Implementation;

namespace Lab5.GContract
{
    public class Glowny:IGlowny
    {
        private IWyswietlacz monitor;
        public Glowny(IWyswietlacz mon)
        {
            this.monitor = mon;
        }
        public Glowny()
        { }

        public string test1()
        {
            return monitor.CzasPodrozy();
        }

        public string test2()
        {
            return monitor.WcisnietyGuzik();
        }
    }
}
