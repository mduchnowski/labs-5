﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;

namespace Lab5.Container
{
    public class ContainerImpl:IContainer
    {
        private Dictionary<Type, Type> impl1;
        private Dictionary<Type, object> impl2;
        private Dictionary<Type, Func<object>> impl3;

        public ContainerImpl()
        {
            this.impl1 = new Dictionary<Type, Type>();
            this.impl2 = new Dictionary<Type, object>();
            this.impl3 = new Dictionary<Type, Func<object>>();
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            this.impl3[typeof(T).GetType()] = provider;
        }

        public void Register<T>(T impl) where T : class
        {
            this.impl2[typeof(T).GetType()] = impl;
        }

        public void Register(Type type)
        {
            foreach (var i in type.GetInterfaces())
            {
                this.impl1[i.GetType()] = type;
            }
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var i in assembly.GetTypes())
            {
                if (i.IsClass)
                {
                    foreach (var j in i.GetType().GetInterfaces())
                    {
                        this.impl1[i] = j.GetType();
                    }
                }
            }
        }

        public object Resolve(Type type)
        {
            System.Type t = type;
            var inst = (object)Activator.CreateInstance(this.impl1[t.GetType()]);

            if (inst == null)
            {
                throw new PK.Container.UnresolvedDependenciesException("Nie spełania warunków");
            }
            else
            {
                return inst as object;
            }
        }

        public T Resolve<T>() where T : class
        {
            var w = this.impl1[typeof(T).GetType()];
            if (w != null)
            {
                var inst = (T)Activator.CreateInstance(w);
                return inst as T;
            }
            var u = this.impl2[typeof(T).GetType()];
            if (u != null)
            {
                return u as T;
            }
            var x = this.impl3[typeof(T).GetType()];
            if (x != null)
            {
                return x() as T;
            }

            throw new PK.Container.UnresolvedDependenciesException("Nie spełnione warunki");
        }
    }
}
