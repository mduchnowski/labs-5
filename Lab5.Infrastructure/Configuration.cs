﻿using System;
using PK.Container;
using Lab5.Container;
using Lab5.Infrastructure;
using Lab5.Contract;
using Lab5.GImplementation;
using lab5.Implementation;
using Lab5.GContract;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        ///
        public static IContainer ConfigureApp()
        {
            IWyswietlacz monitor=new Wyswietlacz();
            IGlowny main=new Glowny(monitor);

            IContainer kontener = new ContainersImpl();

            kontener.Register(monitor);
            kontener.Register(main);

            return kontener;
        }
    }
}
