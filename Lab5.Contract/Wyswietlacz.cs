﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using lab5.Implementation;
using Lab5.DisplayForm;

namespace Lab5.Contract
{
    public class Wyswietlacz:IWyswietlacz
    {
        private DisplayViewModel monitor;
        public Wyswietlacz()
        {
            this.monitor = new DisplayViewModel();
        }

        public string CzasPodrozy()
        {
            monitor.Text = "Czas podróży 4min";
            return monitor.Text;
        }

        public string WcisnietyGuzik()
        {
            monitor.Text = "Wciśnięte piętro 6";
            return monitor.Text;
        }
    }
}
