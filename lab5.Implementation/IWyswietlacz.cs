﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Implementation
{
    public interface IWyswietlacz
    {
        string CzasPodrozy();
        string WcisnietyGuzik();
    }
}
